export interface IControls {
  PlayerOneAttack: string;
  PlayerOneBlock: string;
  PlayerTwoAttack: string;
  PlayerTwoBlock: string;
  PlayerOneCriticalHitCombination: Array<string>;
  PlayerTwoCriticalHitCombination: Array<string>;
}