import { createElement } from '../helpers/domHelper/domHelper';
import { IFighterDetails, IFighter } from './../helpers/mockData/mockData.interface';

export function createFighterPreview(fighter: IFighterDetails, position: string) {
  const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  const fighterElement: HTMLElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterCharacteristics: string[] = ['name', 'health', 'attack', 'defense'];

  const fighterImage: HTMLElement = createFighterImage(fighter);

  const characteristicsContainer: HTMLElement = createElement({
    tagName: 'div',
    className: 'fighter-preview__chars-container',
  });

  fighterElement.append(fighterImage);
  fighterCharacteristics.forEach((characteristic: string) => {
    characteristicsContainer.append(addCharacteristic(characteristic, fighter));
  });

  fighterElement.append(characteristicsContainer);
  return fighterElement;
}

function addCharacteristic(characteristic: string, fighter: IFighterDetails) {
  const container = createElement({
    tagName: 'div',
    className: 'fighter-preview__char-container',
  });

  const characteristicName = createElement({
    tagName: 'span',
    className: 'fighter-preview__char fighter-preview__char__name',
  });
  const characteristicValue = createElement({
    tagName: 'span',
    className: 'fighter-preview__char',
  });

  characteristicName.innerText = characteristic;
  characteristicValue.innerText = String(fighter[characteristic]);

  container.append(characteristicName, characteristicValue);
  return container;
}

export function createFighterImage(fighter: IFighterDetails): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
