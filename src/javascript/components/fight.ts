import { controls } from '../../constants/controls';
import { IFighterDetails } from './../helpers/mockData/mockData.interface';

export async function fight(selectedFighters: Array<IFighterDetails>): Promise<void> {
  const [firstFighter, secondFighter] = selectedFighters;
  const { health: firstFighterHealth } = firstFighter;
  const { health: secondFighterHealth } = secondFighter;
  const firstFighterHPConst = Number(firstFighterHealth);
  const secondFighterHPConst = Number(secondFighterHealth);
  const criticalHitCombinationTime = 10000;
  const firstFighterHealthIndicator = document.getElementById('left-fighter-indicator') as HTMLElement;
  const secondFighterHealthIndicator = document.getElementById('right-fighter-indicator') as HTMLElement;
  const {
    PlayerOneAttack,
    PlayerTwoAttack,
    PlayerOneBlock,
    PlayerTwoBlock,
    PlayerOneCriticalHitCombination,
    PlayerTwoCriticalHitCombination,
  } = controls;

  let firstFighterHP = firstFighterHPConst;
  let secondFighterHP = secondFighterHPConst;
  let isFirstFighterCriticalHitCombinationDisabled = false;
  let isSecondFighterCriticalHitCombinationDisabled = false;

  interface IKeyEvents {
    [key: string]: boolean;
  }

  const keyEvents: IKeyEvents = {};

  document.addEventListener('keydown', keydownHandler, false);
  document.addEventListener('keyup', keyupHandler, false);

  return new Promise((resolve) => {
    document.addEventListener('get-winner', getWinner);

    function getWinner(event: CustomEventInit): void {
      document.removeEventListener('get-winner', getWinner);
      document.removeEventListener('keydown', keydownHandler);
      document.removeEventListener('keyup', keyupHandler);
      resolve(event.detail);
    }
  });

  function keydownHandler(e: KeyboardEvent): void {
    keyEvents[e.code] = true;
    fightAction();
  }

  function keyupHandler(e: KeyboardEvent): void {
    keyEvents[e.code] = false;
  }

  function fightAction(): void {
    if (keyEvents[PlayerOneAttack] && !keyEvents[PlayerOneBlock] && !keyEvents[PlayerTwoBlock]) {
      secondFighterHP = secondFighterHP - getDamage(firstFighter, secondFighter);
      secondFighterHealthIndicator.style.width = (secondFighterHP / secondFighterHPConst) * 100 + '%';
      checkForWinner(firstFighter, secondFighterHP);
    }

    if (keyEvents[PlayerTwoAttack] && !keyEvents[PlayerOneBlock] && !keyEvents[PlayerTwoBlock]) {
      firstFighterHP = firstFighterHP - getDamage(secondFighter, firstFighter);
      firstFighterHealthIndicator.style.width = (firstFighterHP / firstFighterHPConst) * 100 + '%';
      checkForWinner(secondFighter, firstFighterHP);
    }

    if (
      isCriticalHitCombination(PlayerOneCriticalHitCombination) === true &&
      isFirstFighterCriticalHitCombinationDisabled === false
    ) {
      secondFighterHP = secondFighterHP - getResultCombinationDamage(firstFighter);
      secondFighterHealthIndicator.style.width = (secondFighterHP / secondFighterHPConst) * 100 + '%';
      checkForWinner(firstFighter, secondFighterHP);
      isFirstFighterCriticalHitCombinationDisabled = true;
      setTimeout(() => (isFirstFighterCriticalHitCombinationDisabled = false), criticalHitCombinationTime);
    }

    if (
      isCriticalHitCombination(PlayerTwoCriticalHitCombination) === true &&
      isSecondFighterCriticalHitCombinationDisabled === false
    ) {
      firstFighterHP = firstFighterHP - getResultCombinationDamage(secondFighter);
      firstFighterHealthIndicator.style.width = (firstFighterHP / firstFighterHPConst) * 100 + '%';
      checkForWinner(secondFighter, firstFighterHP);
      isSecondFighterCriticalHitCombinationDisabled = true;
      setTimeout(() => (isSecondFighterCriticalHitCombinationDisabled = false), criticalHitCombinationTime);
    }
  }

  function isCriticalHitCombination(combination: Array<string>) {
    let result = true;
    combination.forEach((code) => {
      if (!keyEvents[code]) {
        return (result = false);
      }
    });
    return result;
  }
}

function checkForWinner(attacker: IFighterDetails, defenderHeals: number): void {
  if (defenderHeals <= 0) {
    document.dispatchEvent(
      new CustomEvent('get-winner', {
        detail: {...attacker},
      })
    );
  }
}

function getResultCombinationDamage(fighter: IFighterDetails): number {
  const { attack } = fighter;
  return attack * 2;
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter: IFighterDetails): number {
  const { attack } = fighter;
  const criticalHitChance = getChance();
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: IFighterDetails): number {
  const { defense } = fighter;
  const dodgeChance = getChance();
  const power = defense * dodgeChance;
  return power;
}

function getChance(): number {
  return Math.random() + 1;
}
