import { createElement } from '../helpers/domHelper/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighterDetails } from '../helpers/mockData/mockData.interface';

export function renderArena(selectedFighters: Array<IFighterDetails>): void {
  const root = document.getElementById('root') as HTMLElement;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(selectedFighters).then((fighter: any) => showWinnerModal(fighter)); //todo replace eny type
}

function createArena(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(selectedFighters);
  const fighters = createFighters(selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const [leftFighter, rightFighter] = selectedFighters;
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: 'left' | 'right'): HTMLElement {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const [firstFighter, secondFighter] = selectedFighters;
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: 'left' | 'right'): HTMLElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
