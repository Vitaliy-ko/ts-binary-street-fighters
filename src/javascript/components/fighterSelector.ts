import { createElement } from '../helpers/domHelper/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
const versusImg = require('./../../../resources/versus.png'); //todo solve the error

import { IFighterDetails } from '../helpers/mockData/mockData.interface';

export function createFightersSelector(): (fighterId: string) => void {
  let selectedFighters: Array<IFighterDetails> = [];

  return async (fighterId: string): Promise<void> => { //todo why return asycn function?
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters; //todo move down
    const firstFighter = playerOne ?? fighter;
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  return await fighterService.getFighterDetails(fighterId);
}

function renderSelectedFighters(selectedFighters: Array<IFighterDetails>): void {
  const fightersPreview = document.querySelector('.preview-container___root') as HTMLDivElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Array<IFighterDetails>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Array<IFighterDetails>): void {
  renderArena(selectedFighters);
}
