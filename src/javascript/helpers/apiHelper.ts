import { fightersDetails, fighters } from './mockData/mockData';
import { IFighter, IFighterDetails } from './mockData/mockData.interface'

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

async function callApi<T> (endpoint: string, method: string):Promise<T> {
  const url: string = API_URL + endpoint;
  const options: {method: string} = {
    method
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<IFighter[] | never> {
  const response: any = endpoint === 'fighters.json' ? fighters as Array<IFighter> : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighterDetails | undefined {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);
  return fightersDetails.find((it: IFighterDetails) => it._id === id);
}

export { callApi };
