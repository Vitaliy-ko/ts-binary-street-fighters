export interface IHTMLElementParams {
  tagName: string;
  className?: string; 
  attributes?: any
}