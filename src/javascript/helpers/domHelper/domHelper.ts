import { IHTMLElementParams } from './domHelper.interface'


export function createElement({ tagName, className, attributes = {} }:IHTMLElementParams): HTMLElement {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key: string) => element.setAttribute(key, attributes[key]));

  return element;
}

