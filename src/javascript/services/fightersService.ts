import { callApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetails } from './../helpers/mockData/mockData.interface'

interface IFighterService {
  getFighters(): Promise<IFighter[] | never>;
  getFighterDetails(id:string): Promise<IFighterDetails | never>;

}

class FighterService implements IFighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET') as IFighter[];

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
  async getFighterDetails(id: string) {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi<IFighterDetails>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService: IFighterService = new FighterService();
